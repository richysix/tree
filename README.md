Tree

================================================================================


Tree::GenomicIntervalTree
	Implementation of an Interval Tree for genomic intervals.

Tree::AnnotationTree
	Subclass of GenomicIntervalTree specifically for genome annotation

